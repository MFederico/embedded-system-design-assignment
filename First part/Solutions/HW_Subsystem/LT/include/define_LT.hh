#ifndef __define_LT_h__
#define __define_LT_h__

#include <systemc.h>

struct iostruct
{
    sc_uint<32> word0;
    sc_uint<32> word1;
    sc_uint<32> key0;
    sc_uint<32> key1;
    sc_uint<32> key2;
    sc_uint<32> key3;
    bool mode;
    sc_uint<32> res0;
    sc_uint<32> res1;
};

#define ADDRESS_TYPE int
#define DATA_TYPE iostruct

#endif
