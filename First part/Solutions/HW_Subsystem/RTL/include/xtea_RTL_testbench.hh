#ifndef _TB_RTL_H_
#define _TB_RTL_H_

#include <systemc.h>
#define PERIOD 10
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdint.h>
#include <string>

class tb_rtl : sc_core::sc_module
{

  public:
    sc_out<bool> clk;
    sc_out<sc_logic> rst;

    sc_out<sc_uint<32> > w0, w1, k0, k1, k2, k3;

    sc_out<sc_logic> din_rdy;

    sc_in<sc_uint<32> > result0, result1;
    sc_in<sc_logic> dout_rdy;
    sc_out<bool> mode;

    SC_HAS_PROCESS(tb_rtl);
    tb_rtl(sc_module_name name);
    ~tb_rtl() {}

  private:
    void run();
    void clk_gen();
};
#endif
