#include "xtea_RTL_testbench.hh"


tb_rtl::tb_rtl(sc_module_name name)
{
    SC_THREAD(run);
    sensitive << clk.pos();

    SC_THREAD(clk_gen);
}

void tb_rtl::clk_gen()
{
    while (true)
    {
        clk.write(true);
        wait(PERIOD / 2, SC_NS);
        clk.write(false);
        wait(PERIOD / 2, SC_NS);
    }
}

void tb_rtl::run()
{
	//Data used to test the module, it's in the form:
    const uint32_t tests[56] = {
        0xefa5a6d6, 0x7c478c1c, 0x83289ca0, 0x2a250bd8, 0x93a5a855, 0xa09951d6,
        0xb804fcc3, 0x36737612, 0xb45dd7c6, 0x70641284, 0x5d7953ed, 0xb42c05c5,
        0x6695e064, 0x73e9b8db, 0xccf49510, 0xcf872c65, 0x6762dc91, 0x2901a81c,
        0xb8198578, 0x648def27, 0x63026400, 0xd0ad018f, 0x2a5e0ad1, 0xed059d1a,
        0xc3a1b25e, 0xb8e405a2, 0x53d90cad, 0x2161d454, 0x68ed40ad, 0xfef97cbd,
        0x62dd3c05, 0xfec3fbbe, 0xb1655848, 0x177b62c0, 0xae03681f, 0x71f60c08,
        0xc06feab4, 0x39393da,  0x52c64b2,  0x535fd048, 0xd7e3cd30, 0x42345697,
        0x74dddc21, 0x7ff065a6, 0xc4446d16, 0x1a0abfb8, 0x4fb3a942, 0xe88160e7,
        0x5d151ec7, 0x300dcd84, 0x9bcd7e5d, 0x9b097746};

    int j = 0;
    uint32_t key0, key1, key2, key3, res0, res1;
    uint32_t word0, word1, output0, output1;
    for (int i = 0; i < 52; i += 4)
    {
        word0 = tests[i];
        word1 = tests[i + 1];
        output0 = tests[i + 2];
        output1 = tests[i + 3];

        key0 = 0x6a1d78c8;
        key1 = 0x8c86d67f;
        key2 = 0x2a65bfbe;
        key3 = 0xb4bd6e46;

        w0.write(word0);
        w1.write(word1);
        k0.write(key0);
        k1.write(key1);
        k2.write(key2);
        k3.write(key3);
        mode.write(0);//encrypt
        din_rdy.write(sc_logic(1));
		
        wait();
		//Wait for the module to finish encrypt
        while (dout_rdy.read() != sc_logic(1))
        {
            wait();
        }

        din_rdy.write(sc_logic(0));
        res0 = result0.read();
        res1 = result1.read();

        cout << "Encryption of " << hex << word0 << " and " << hex << word1
             << " is " << hex << res0 << " and " << hex << res1 << endl;

        if (res0 != output0 || res1 != output1) {
            cout << " Encryption error with words " << word0 << " " << word1
                 << " stopping simulation" << endl;
            sc_stop();
        }

        w0.write(res0);
        w1.write(res1);
        k0.write(key0);
        k1.write(key1);
        k2.write(key2);
        k3.write(key3);
        mode.write(1);//decrypt
        din_rdy.write(sc_logic(1));

        wait();
		//Wait for the module to finish decrypt
        while (dout_rdy.read() != sc_logic(1))
        {
            wait();
        }

        din_rdy.write(sc_logic(0));

        cout << "Decryption of " << hex << res0 << " and " << hex << res1
             << " is " << hex << result0.read() << " and " << hex
             << result1.read() << endl;

        if (result0.read() != word0 || result1.read() != word1) {
            cout << " Decryption error with words " << word0 << " " << word1
                 << " stopping simulation" << endl;
            sc_stop();
        }

		
    }

    cout << "All tests passed" << endl;
    sc_stop();
}
