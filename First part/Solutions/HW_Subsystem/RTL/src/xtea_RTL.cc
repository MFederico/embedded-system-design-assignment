#include "xtea_RTL.hh"

xteaModule::xteaModule(const sc_module_name &name)
    : sc_module(name)
    , din_rdy("din_rdy")
    , dout_rdy("dout_rdy")
    , w0("w0")
    , w1("w1")
    , k0("k0")
    , k1("k1")
    , k2("k2")
    , k3("k3")
    , result0("result0")
    , result1("result1")
    , mode("mode")
    , clk("clk")
    , rst("rst")

{
	//Initialize internal signals
    i = new sc_signal<sc_uint<6> >;
    cond_d = new sc_signal<sc_uint<2> >;
    cond_e = new sc_signal<sc_uint<2> >;
    sum = new sc_signal<sc_uint<64> >;

    keys[0] = new sc_signal<sc_uint<32> >;
    keys[1] = new sc_signal<sc_uint<32> >;
    keys[2] = new sc_signal<sc_uint<32> >;
    keys[3] = new sc_signal<sc_uint<32> >;

    i->write(0);
    sum->write(0);
    delta = 0x9e3779b9;

    v0 = new sc_signal<sc_uint<32> >;
    v1 = new sc_signal<sc_uint<32> >;
    v0->write(0);
    v1->write(0);

    SC_METHOD(dataPath);
    sensitive << clk.pos() << rst;
    SC_METHOD(fsm);
    sensitive << din_rdy << STATE;
}

xteaModule::~xteaModule()
{
    delete i;
    delete cond_d;
    delete cond_e;
    delete sum;
    delete keys[0];
    delete keys[1];
    delete keys[2];
    delete keys[3];
    delete v0;
    delete v1;
}

void xteaModule::fsm()
{

    switch (STATE)
    {
        case R:
            if (din_rdy.read() == 1) {
                NEXT_STATE.write(IR);
            }
            else
            {
                NEXT_STATE.write(R);
            }
            break;

        case IR:
            NEXT_STATE.write(D0);
            break;

        case D0:
            if (mode.read() == 1) {
                NEXT_STATE.write(DEC);
            }
            else
            {
                NEXT_STATE.write(ENC);
            }
            break;

        case ENC:
            if (i->read() < 32) {
                NEXT_STATE.write(A);
            }
            else
            {
                NEXT_STATE.write(END);
            }
            break;

        case A:
            NEXT_STATE.write(D1);
            break;

        case D1:
            NEXT_STATE.write(B);
            break;

        case B:
            NEXT_STATE.write(ENC);
            break;

        case DEC:
            if (i->read() < 32) {
                NEXT_STATE.write(C);
            }

            else
            {
                NEXT_STATE.write(END);
            }
            break;

        case C:
            NEXT_STATE.write(D2);
            break;

        case D2:
            NEXT_STATE.write(D);
            break;

        case D:
            NEXT_STATE.write(DEC);
            break;

        case END:
            NEXT_STATE.write(R);
            break;

        default:
            NEXT_STATE.write(R);
            break;
    }
}

void xteaModule::dataPath()
{

    uint32_t c = 0;

    if (rst.read() == sc_logic(1) or din_rdy.read() == 0) {
        STATE.write(R);
    }

    else if (clk.read() == 1)
    {

        STATE.write(NEXT_STATE);

        switch (STATE)
        {
            case R:
                sum->write(0);

                i->write(0);

                cond_e->write(0);
                cond_d->write(0);

                dout_rdy.write(sc_logic(0));
                result0.write(0);
                result1.write(0);
                break;

            case IR:
                v0->write(w0.read());
                v1->write(w1.read());
                keys[0]->write(k0.read());
                keys[1]->write(k1.read());
                keys[2]->write(k2.read());
                keys[3]->write(k3.read());

                break;

            case D0:
                cond_e->write(sum->read() & 3);
                cond_d->write(((delta * 32) >> 11) & 3);
                break;

            case D1:
                i->write(i->read() + 1);
                cond_e->write((sum->read() >> 11) & 3);
                break;

            case DEC:
                sum->write(delta * (32 - i->read()));
                i->write(i->read() + 1);
                break;

            case ENC:
                break;
            case D2:

                cond_d->write(sum->read() & 3);
                break;

            case A:

                c = (((v1->read() << 4) ^ (v1->read() >> 5)) + v1->read()) ^
                    (sum->read() + keys[cond_e->read()]->read());
                c += v0->read();
                v0->write(c);
                c = sum->read();
                sum->write(c + delta);
                break;

            case B:

                c = (((v0->read() << 4) ^ (v0->read() >> 5)) + v0->read()) ^
                    (sum->read() + keys[cond_e->read()]->read());
                c += v1->read();
                v1->write(c);
                cond_e->write(sum->read() & 3);
                break;

            case C:

                c = (((v0->read() << 4) ^ (v0->read() >> 5)) + v0->read()) ^
                    (sum->read() + keys[cond_d->read()]->read());
                c = v1->read() - c;
                v1->write(c);
                sum->write(delta * (32 - i->read()));
                break;

            case D:

                c = (((v1->read() << 4) ^ (v1->read() >> 5)) + v1->read()) ^
                    (sum->read() + keys[cond_d->read()]->read());
                c = v0->read() - c;
                v0->write(c);
                cond_d->write((sum->read() >> 11) & 3);
                break;

            case END:
                dout_rdy.write(sc_logic(1));
                result0.write(v0->read());
                result1.write(v1->read());
                break;
        }
    }
}
