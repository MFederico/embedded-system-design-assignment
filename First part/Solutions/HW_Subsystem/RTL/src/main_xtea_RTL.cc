#include "xtea_RTL.hh"
#include "xtea_RTL_testbench.hh"
#define TRACE //Decomment to trace signals

int sc_main(int argc, char *argv[])
{
	//declare signals to connect the two modules
    sc_signal<bool> clk;
    sc_signal<sc_logic> rst;

    sc_signal<sc_uint<32> > w0, w1, k0, k1, k2, k3;

    sc_signal<bool> mode;
    sc_signal<sc_logic> din_rdy;

    sc_signal<sc_uint<32> > result0, result1;
    sc_signal<sc_logic> dout_rdy;

    sc_trace_file *fp;

#ifdef TRACE
    fp = sc_create_vcd_trace_file("wave");
#endif

    //bind testbench ports
    tb_rtl *tb = new tb_rtl("tb");
    tb->clk(clk);
    tb->rst(rst);
    tb->w0(w0);
    tb->w1(w1);
    tb->k0(k0);
    tb->k1(k1);
    tb->k2(k2);
    tb->k3(k3);
    tb->result0(result0);
    tb->result1(result1);
    tb->din_rdy(din_rdy);
    tb->dout_rdy(dout_rdy);
    tb->mode(mode);

	//bind xtea ports
    xteaModule *xtea = new xteaModule("xtea");
    xtea->clk(clk);
    xtea->rst(rst);
    xtea->w0(w0);
    xtea->w1(w1);
    xtea->k0(k0);
    xtea->k1(k1);
    xtea->k2(k2);
    xtea->k3(k3);
    xtea->result0(result0);
    xtea->result1(result1);
    xtea->din_rdy(din_rdy);
    xtea->dout_rdy(dout_rdy);
    xtea->mode(mode);

#ifdef TRACE
    sc_trace(fp, clk, "clk");
    sc_trace(fp, xtea->STATE, "state");
    sc_trace(fp, result0, "result0");
    sc_trace(fp, result1, "result1");
#endif

    sc_start();

#ifdef TRACE
    if (fp != NULL) {
        sc_close_vcd_trace_file(fp);
    }
#endif

    delete tb;
    delete xtea;
    return 0;
};
