#include "xtea_UT.hh"

xtea_UT::xtea_UT(sc_module_name name_)
    : sc_module(name_)
    , target_socket("target_socket")
    , pending_transaction(NULL)
{

    target_socket(*this);
}

void xtea_UT::b_transport(tlm::tlm_generic_payload &trans, sc_time &t)
{

    ioDataStruct = *((iostruct *)trans.get_data_ptr());

    if (trans.is_write()) {

        if (ioDataStruct.mode == false) {
            xtea_encrypt(ioDataStruct.word0, ioDataStruct.word1,ioDataStruct.key0, ioDataStruct.key1,ioDataStruct.key2, ioDataStruct.key3);
        }
        else
        {
            xtea_decrypt(ioDataStruct.word0, ioDataStruct.word1,ioDataStruct.key0, ioDataStruct.key1,ioDataStruct.key2, ioDataStruct.key3);
        }
        trans.set_response_status(tlm::TLM_OK_RESPONSE);
        ioDataStruct.res0 = res0;
        ioDataStruct.res1 = res1;
        *((iostruct *)trans.get_data_ptr()) = ioDataStruct;
    }
    else if (trans.is_read())
    {
        ioDataStruct.res0 = res0;
        ioDataStruct.res1 = res1;
        *((iostruct *)trans.get_data_ptr()) = ioDataStruct;
    }
}

bool xtea_UT::get_direct_mem_ptr(tlm::tlm_generic_payload &trans,tlm::tlm_dmi &dmi_data)
{
    return false;
}

tlm::tlm_sync_enum xtea_UT::nb_transport_fw(tlm::tlm_generic_payload &trans,tlm::tlm_phase &phase, sc_time &t)
{
    return tlm::TLM_COMPLETED;
}

unsigned int xtea_UT::transport_dbg(tlm::tlm_generic_payload &trans)
{
    return 0;
}

void xtea_UT::xtea_encrypt(sc_uint<32> word0, sc_uint<32> word1,sc_uint<32> key0, sc_uint<32> key1, sc_uint<32> key2,sc_uint<32> key3)
{
    sc_uint<64> sum;
    sc_uint<32> i, delta, v0, v1, temp;
    v0 = word0;
    v1 = word1;
    sum = 0;
    res0 = 0;
    res1 = 0;
    delta = 0x9e3779b9;

    for (i = 0; i < 32; i++)
    {

        if ((sum & 3) == 0)
            temp = key0;
        else if ((sum & 3) == 1)
            temp = key1;
        else if ((sum & 3) == 2)
            temp = key2;
        else
            temp = key3;

        v0 += (((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + temp);

        sum += delta;

        if (((sum >> 11) & 3) == 0)
            temp = key0;
        else if (((sum >> 11) & 3) == 1)
            temp = key1;
        else if (((sum >> 11) & 3) == 2)
            temp = key2;
        else
            temp = key3;

        v1 += (((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + temp);
    }

    res0 = v0;
    res1 = v1;
}

void xtea_UT::xtea_decrypt(sc_uint<32> word0, sc_uint<32> word1,sc_uint<32> key0, sc_uint<32> key1, sc_uint<32> key2,sc_uint<32> key3)
{
    sc_uint<64> sum;
    sc_uint<32> i, delta, v0, v1, temp;
    v0 = word0;
    v1 = word1;
    res0 = 0;
    res1 = 0;
    delta = 0x9e3779b9;
    sum = delta * 32;

    for (i = 0; i < 32; i++)
    {

        if (((sum >> 11) & 3) == 0)
            temp = key0;
        else if (((sum >> 11) & 3) == 1)
            temp = key1;
        else if (((sum >> 11) & 3) == 2)
            temp = key2;
        else
            temp = key3;

        v1 -= (((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + temp);

        sum -= delta;

        if ((sum & 3) == 0)
            temp = key0;
        else if ((sum & 3) == 1)
            temp = key1;
        else if ((sum & 3) == 2)
            temp = key2;
        else
            temp = key3;

        v0 -= (((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + temp);
    }

    res0 = v0;
    res1 = v1;
}
