#include "xtea_AT4_testbench.hh"

void xtea_AT4_testbench::invalidate_direct_mem_ptr(uint64 start_range,
                                                   uint64 end_range)
{
}

tlm::tlm_sync_enum
xtea_AT4_testbench::nb_transport_bw(tlm::tlm_generic_payload &trans,
                                    tlm::tlm_phase &phase, sc_time &t)
{

    if (response_pending != true || phase != tlm::BEGIN_RESP) {

        trans.set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
        return tlm::TLM_COMPLETED;
    }

    available_response.notify();
    phase = tlm::END_RESP;

    return tlm::TLM_COMPLETED;
}

void xtea_AT4_testbench::run()
{
    sc_time local_time = SC_ZERO_TIME;
    iostruct xtea_packet;
    tlm::tlm_generic_payload payload;

    const uint32_t tests[56] = {
        0xefa5a6d6, 0x7c478c1c, 0x83289ca0, 0x2a250bd8, 0x93a5a855, 0xa09951d6,
        0xb804fcc3, 0x36737612, 0xb45dd7c6, 0x70641284, 0x5d7953ed, 0xb42c05c5,
        0x6695e064, 0x73e9b8db, 0xccf49510, 0xcf872c65, 0x6762dc91, 0x2901a81c,
        0xb8198578, 0x648def27, 0x63026400, 0xd0ad018f, 0x2a5e0ad1, 0xed059d1a,
        0xc3a1b25e, 0xb8e405a2, 0x53d90cad, 0x2161d454, 0x68ed40ad, 0xfef97cbd,
        0x62dd3c05, 0xfec3fbbe, 0xb1655848, 0x177b62c0, 0xae03681f, 0x71f60c08,
        0xc06feab4, 0x39393da,  0x52c64b2,  0x535fd048, 0xd7e3cd30, 0x42345697,
        0x74dddc21, 0x7ff065a6, 0xc4446d16, 0x1a0abfb8, 0x4fb3a942, 0xe88160e7,
        0x5d151ec7, 0x300dcd84, 0x9bcd7e5d, 0x9b097746};

    sc_uint<32> res1, res0, output0, output1;
    for (int i = 0; i < 52; i += 4)
    {
        xtea_packet.word0 = tests[i];
        xtea_packet.word1 = tests[i + 1];
        xtea_packet.key0 = 0x6a1d78c8;
        xtea_packet.key1 = 0x8c86d67f;
        xtea_packet.key2 = 0x2a65bfbe;
        xtea_packet.key3 = 0xb4bd6e46;
        xtea_packet.mode = false;//encrypt

        output0 = tests[i + 2];
        output1 = tests[i + 3];

        payload.set_data_ptr((unsigned char *)&xtea_packet);
        payload.set_address(0);
        payload.set_write();

        tlm::tlm_phase phase = tlm::BEGIN_REQ;

		//Begin encrypt request


		
		// start write transaction
        tlm::tlm_sync_enum result =
            initiator_socket->nb_transport_fw(payload, phase, local_time);

        if (result == tlm::TLM_COMPLETED || phase != tlm::END_REQ) {
            sc_stop();
        }

        response_pending = true;
        wait(available_response);
        response_pending = false;

        
        phase = tlm::BEGIN_REQ;
        payload.set_address(0);
        payload.set_data_ptr((unsigned char *)&xtea_packet);
        payload.set_read();
		// start read transaction
        result = initiator_socket->nb_transport_fw(payload, phase, local_time);

        if (result == tlm::TLM_COMPLETED || phase != tlm::END_REQ) {
            sc_stop();
        }

        response_pending = true;
        wait(available_response);
        response_pending = false;

        if (payload.get_response_status() == tlm::TLM_OK_RESPONSE) {
            res0 = xtea_packet.res0;
            res1 = xtea_packet.res1;
            cout << "Encryption of " << hex << tests[i] << " and " << hex
                 << tests[i + 1] << " is " << hex << res0 << " and " << hex
                 << res1 << endl;

            if (res0 != output0 || res1 != output1) {
                cout << " Encryption error with words " << tests[i] << " "
                     << tests[i + 1] << " stopping simulation" << endl;
                sc_stop();
            }
        }

		//Encrypt ended, begin decrypt

        xtea_packet.mode = true;//decrypt
        xtea_packet.word0 = res0;
        xtea_packet.word1 = res1;
        payload.set_data_ptr((unsigned char *)&xtea_packet);
        payload.set_address(0);
        payload.set_write();

        phase = tlm::BEGIN_REQ;

		// start write transaction
        result = initiator_socket->nb_transport_fw(payload, phase, local_time);

        if (result == tlm::TLM_COMPLETED || phase != tlm::END_REQ) {
            sc_stop();
        }

        response_pending = true;
        wait(available_response);
        response_pending = false;

       
        phase = tlm::BEGIN_REQ;
        payload.set_address(0);
        payload.set_data_ptr((unsigned char *)&xtea_packet);
        payload.set_read();
		// start read transaction
        result = initiator_socket->nb_transport_fw(payload, phase, local_time);

        if (result == tlm::TLM_COMPLETED || phase != tlm::END_REQ) {
            sc_stop();
        }

        response_pending = true;
        wait(available_response);
        response_pending = false;

        if (payload.get_response_status() == tlm::TLM_OK_RESPONSE) {
            res0 = xtea_packet.res0;
            res1 = xtea_packet.res1;
            cout << "Decryption of " << hex << res0 << " and " << hex << res1
                 << " is " << hex << xtea_packet.res0 << " and " << hex
                 << xtea_packet.res1 << endl;

            if (xtea_packet.res0 != tests[i] ||
                xtea_packet.res1 != tests[i + 1])
            {
                cout << " Decryption error with words " << tests[i] << " "
                     << tests[i + 1] << " stopping simulation" << endl;
                sc_stop();
            }
        }
    }
	cout<<"All tests passed"<<endl;
    sc_stop();
}

xtea_AT4_testbench::xtea_AT4_testbench(sc_module_name name) : sc_module(name)
{

    initiator_socket(*this);

    SC_THREAD(run);
}
