#include "xtea_AT4.hh"

xtea_AT4::xtea_AT4(sc_module_name name_)
    : sc_module(name_)
    , target_socket("target_socket")
    , pending_transaction(NULL)
{

    target_socket(*this);
    SC_THREAD(IOPROCESS);
}

void xtea_AT4::b_transport(tlm::tlm_generic_payload &trans, sc_time &t) {}

bool xtea_AT4::get_direct_mem_ptr(tlm::tlm_generic_payload &trans,
                                  tlm::tlm_dmi &dmi_data)
{
    return false;
}

unsigned int xtea_AT4::transport_dbg(tlm::tlm_generic_payload &trans)
{
    return 0;
}

tlm::tlm_sync_enum xtea_AT4::nb_transport_fw(tlm::tlm_generic_payload &trans,
                                             tlm::tlm_phase &phase, sc_time &t)
{
    
    if (pending_transaction != NULL || phase != tlm::BEGIN_REQ) {
        trans.set_response_status(tlm::TLM_GENERIC_ERROR_RESPONSE);
        return tlm::TLM_COMPLETED;
    }

    pending_transaction = &trans;
    ioDataStruct = *((iostruct *)trans.get_data_ptr());

    phase = tlm::END_REQ;

	//Send notify to the thread waiting on the event
    
    io_event.notify();

    return tlm::TLM_UPDATED;
}

void xtea_AT4::IOPROCESS()
{

    sc_time timing_annotation;

    while (true)
    {
        wait(io_event);
		//Wait 100 ns to simulate the time spent to process the request
        wait(100, SC_NS);

        if (pending_transaction->is_write()) {

            if (ioDataStruct.mode) { // decrypt
                xtea_decrypt(ioDataStruct.word0, ioDataStruct.word1,
                             ioDataStruct.key0, ioDataStruct.key1,
                             ioDataStruct.key2, ioDataStruct.key3);
            }
            else
            { // encrypt
                xtea_encrypt(ioDataStruct.word0, ioDataStruct.word1,
                             ioDataStruct.key0, ioDataStruct.key1,
                             ioDataStruct.key2, ioDataStruct.key3);
            }
        }
        pending_transaction->set_response_status(tlm::TLM_OK_RESPONSE);

        *((iostruct *)pending_transaction->get_data_ptr()) = ioDataStruct;

        tlm::tlm_phase phase = tlm::BEGIN_RESP;

        target_socket->nb_transport_bw(*pending_transaction, phase,
                                       timing_annotation);

        pending_transaction = NULL;
    }
}

void xtea_AT4::xtea_encrypt(sc_uint<32> word0, sc_uint<32> word1,
                            sc_uint<32> key0, sc_uint<32> key1,
                            sc_uint<32> key2, sc_uint<32> key3)
{
    sc_uint<64> sum;
    sc_uint<32> i, delta, v0, v1, temp;
    v0 = word0;
    v1 = word1;
    sum = 0;

    delta = 0x9e3779b9;

    timing_annotation += sc_time(100, SC_NS);
    for (i = 0; i < 32; i++)
    {

        if ((sum & 3) == 0)
            temp = key0;
        else if ((sum & 3) == 1)
            temp = key1;
        else if ((sum & 3) == 2)
            temp = key2;
        else
            temp = key3;

        v0 += (((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + temp);

        sum += delta;

        if (((sum >> 11) & 3) == 0)
            temp = key0;
        else if (((sum >> 11) & 3) == 1)
            temp = key1;
        else if (((sum >> 11) & 3) == 2)
            temp = key2;
        else
            temp = key3;

        v1 += (((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + temp);
    }

    ioDataStruct.res0 = v0;
    ioDataStruct.res1 = v1;
}

void xtea_AT4::xtea_decrypt(sc_uint<32> word0, sc_uint<32> word1,
                            sc_uint<32> key0, sc_uint<32> key1,
                            sc_uint<32> key2, sc_uint<32> key3)
{
    sc_uint<64> sum;
    sc_uint<32> i, delta, v0, v1, temp;
    v0 = word0;
    v1 = word1;
    res0 = 0;
    res1 = 0;
    delta = 0x9e3779b9;
    sum = delta * 32;

    timing_annotation += sc_time(100, SC_NS);

    for (i = 0; i < 32; i++)
    {

        if (((sum >> 11) & 3) == 0)
            temp = key0;
        else if (((sum >> 11) & 3) == 1)
            temp = key1;
        else if (((sum >> 11) & 3) == 2)
            temp = key2;
        else
            temp = key3;

        v1 -= (((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + temp);

        sum -= delta;

        if ((sum & 3) == 0)
            temp = key0;
        else if ((sum & 3) == 1)
            temp = key1;
        else if ((sum & 3) == 2)
            temp = key2;
        else
            temp = key3;

        v0 -= (((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + temp);
    }

    ioDataStruct.res0 = v0;
    ioDataStruct.res1 = v1;
}
