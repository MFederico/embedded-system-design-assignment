#include "water_tank_transactor.hh"

water_tank_transactor::water_tank_transactor(sc_module_name name)
    : sc_module(name)
    , initiator_socket("initiator_socket")
    , pending_transaction(NULL)
    , in("wt_in")
{
    initiator_socket(*this);
    SC_THREAD(wait);
    sensitive << in;
 
}

void water_tank_transactor::invalidate_direct_mem_ptr(uint64 start_range,
                                                      uint64 end_range)
{
}

tlm::tlm_sync_enum
water_tank_transactor::nb_transport_bw(tlm::tlm_generic_payload &trans,
                                       tlm::tlm_phase &phase, sc_time &t)
{
    return tlm::TLM_COMPLETED;
}

void water_tank_transactor::wait()
{

    
    tankstruct tank_packet;
    tlm::tlm_generic_payload payload;
    sc_time local_time = SC_ZERO_TIME;

    while (true)
    {
        tank_packet.level = in.read();
        payload.set_data_ptr((unsigned char *)&tank_packet);
        payload.set_write();
        initiator_socket->b_transport(payload, local_time); //call b_transport on the TLM controller
        sc_core::wait();//Wait for the next value written on the input port by the water tank
    }
}

void water_tank_transactor::end_of_elaboration() {}

void water_tank_transactor::reset() {}

water_tank_transactor::~water_tank_transactor()
{
   
}
