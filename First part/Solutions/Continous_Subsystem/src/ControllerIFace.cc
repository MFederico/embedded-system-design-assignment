#include "ControllerIFace.hh"

controller_iface::controller_iface(sc_core::sc_module_name)
    :

    inp_command("inp_command")
    , inp_thresh("inp_thresh")
    , out_command("out_command")
    , out_thresh("out_thresh")

{
}

controller_iface::~controller_iface() {}

void controller_iface::processing()
{
    out_command.write(inp_command.read());
    out_thresh.write(inp_thresh.read());
}

void controller_iface::set_attribute() {}
