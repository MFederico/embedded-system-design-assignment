#include "valve_transactor.hh"

valve_transactor::valve_transactor(sc_module_name name)
    : sc_module(name)
    , target_socket("target_socket")
    , pending_transaction(NULL)
    , out_command("out_command")
    , out_thresh("out_thresh")
{
    target_socket(*this);
}

void valve_transactor::b_transport(tlm::tlm_generic_payload &trans, sc_time &t)
{
    ioDataStruct = *((iostruct *)trans.get_data_ptr());
    transduce();
    trans.set_data_ptr((unsigned char *)&ioDataStruct);
}

bool valve_transactor::get_direct_mem_ptr(tlm::tlm_generic_payload &trans,
                                          tlm::tlm_dmi &dmi_data)
{
    return false;
}

tlm::tlm_sync_enum
valve_transactor::nb_transport_fw(tlm::tlm_generic_payload &trans,
                                  tlm::tlm_phase &phase, sc_time &t)
{
    return tlm::TLM_COMPLETED;
}

unsigned int valve_transactor::transport_dbg(tlm::tlm_generic_payload &trans)
{
    return 0;
}

void valve_transactor::transduce()
{
    //Write to output ports
    out_thresh.write(ioDataStruct.th);
    out_command.write(ioDataStruct.c);
    
}

void valve_transactor::end_of_elaboration() {}

void valve_transactor::reset() {}

valve_transactor::~valve_transactor() {}
