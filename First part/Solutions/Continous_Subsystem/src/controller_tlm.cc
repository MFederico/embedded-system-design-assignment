#include "controller_tlm.hh"

controller_tlm::controller_tlm(sc_module_name name)
    : sc_module(name)
    , initiator_socket("initiator_socket")
    , target_socket("target_socket")
    , threshold(0.7)
    , waterLevel(0.0)
{

    initiator_socket(*this);
    target_socket(*this);
    SC_THREAD(run);
}

void controller_tlm::invalidate_direct_mem_ptr(uint64 start_range,
                                               uint64 end_range)
{
}

bool controller_tlm::get_direct_mem_ptr(tlm::tlm_generic_payload &trans,
                                        tlm::tlm_dmi &dmi_data)
{
    return false;
}

tlm::tlm_sync_enum
controller_tlm::nb_transport_bw(tlm::tlm_generic_payload &trans,
                                tlm::tlm_phase &phase, sc_time &t)
{
    return tlm::TLM_COMPLETED;
}

tlm::tlm_sync_enum
controller_tlm::nb_transport_fw(tlm::tlm_generic_payload &trans,
                                tlm::tlm_phase &phase, sc_time &t)
{
    return tlm::TLM_COMPLETED;
}

unsigned int controller_tlm::transport_dbg(tlm::tlm_generic_payload &trans)
{
    return 0;
}

void controller_tlm::b_transport(tlm::tlm_generic_payload &trans, sc_time &t)
{
    
    tankStruct = *((tankstruct *)trans.get_data_ptr());
    trans.set_data_ptr((unsigned char *)&tankStruct);
    waterLevel = tankStruct.level;

}

void controller_tlm::run()
{
    iostruct valve_packet;
    tlm::tlm_generic_payload payload;
    local_time = SC_ZERO_TIME;
   
    while (true)
    {

        if (waterLevel > 8.8) {
           
            valve_packet.c = CLOSE;
            threshold *= 0.7;
            valve_packet.th = threshold;
            wait(5, SC_SEC);
        }

        else if (waterLevel < 5.0)
        {
            
            valve_packet.c = OPEN;
            threshold *= 1.1;
            valve_packet.th = threshold;
            wait(5, SC_SEC);
        }

        else
        {
            valve_packet.c = IDLE;
            valve_packet.th = threshold;
        }

        payload.set_data_ptr((unsigned char *)&valve_packet);
        payload.set_address(0);
        payload.set_write();

        //call b_transport on the transactor

        initiator_socket->b_transport(payload, local_time);

		//wait to make the controller slower than the valve and water tank
        wait(1.0, SC_MS);
       
    }
}
