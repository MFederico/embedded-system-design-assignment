#include "System.hh"

System::System(sc_core::sc_module_name)
    : command_vt_ci("command_vt_ci")
    , threshold_vt_ci("threshold_vt_ci")
    , sig_out_valve("valve-tank")
    , sig_out_wt("wt-transactor")
    , comm_cif_valve("comm_cif_valve")
    , thresh_cif_valve("thresh_cif_valve")
{
	
	//Instantiate all the components
    v = new valve_tdf("valve_tdf");
    ct = new controller_tlm("controller_tlm");
    vt = new valve_transactor("valve_transactor");
    wt = new water_tank_lsf("water_tank_lsf");
    wtt = new water_tank_transactor("water_tank_transactor");
    cif = new controller_iface("cif");
    

    // Bind valve_transactor
    vt->out_command(command_vt_ci);
    vt->out_thresh(threshold_vt_ci);
    

    // Bind interface module
	cif->out_thresh(thresh_cif_valve);
    cif->out_command(comm_cif_valve);
	cif->inp_command(command_vt_ci);
	cif->inp_thresh(threshold_vt_ci);

	// Bind valve
    v->command(comm_cif_valve);
    v->threshold(thresh_cif_valve);
	v->out(sig_out_valve);

    // Controller TLM
    ct->initiator_socket(vt->target_socket);
    
	//Bind Water Tank LSF
    wt->in(sig_out_valve);
    wt->out(sig_out_wt);

    // Bind water_tank_transactor
    wtt->in(sig_out_wt);
    wtt->initiator_socket(ct->target_socket);

}

System::~System()
{
    delete v;
    delete vt;
    delete ct;
    delete wt;
    delete wtt;
    delete cif;
}
