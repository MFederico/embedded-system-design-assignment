#ifndef VALVE_TRANSACTOR_HH
#define VALVE_TRANSACTOR_HH

#include "define_LT.hh"
#include "systemc-ams.h"
#include <tlm.h>

class valve_transactor : public sc_module,
                         public virtual tlm::tlm_fw_transport_if<>
{
  public:
    tlm::tlm_target_socket<> target_socket; 

    virtual void b_transport(tlm::tlm_generic_payload &trans, sc_time &t);
    virtual bool get_direct_mem_ptr(tlm::tlm_generic_payload &trans,
                                    tlm::tlm_dmi &dmi_data);

    virtual unsigned int transport_dbg(tlm::tlm_generic_payload &trans);

    virtual tlm::tlm_sync_enum nb_transport_fw(tlm::tlm_generic_payload &trans,
                                               tlm::tlm_phase &phase,
                                               sc_time &t);

    void end_of_elaboration();
    void reset();

    iostruct ioDataStruct;

    tlm::tlm_generic_payload *pending_transaction;

    sc_time timing_annotation;

    void transduce();

    SC_HAS_PROCESS(valve_transactor);

    valve_transactor(sc_module_name name);
    ~valve_transactor();

    
    sc_out<COMMAND> out_command;
    sc_out<double> out_thresh;
};

#endif
