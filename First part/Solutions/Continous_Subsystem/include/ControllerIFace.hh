#ifndef CONTROLLER_IFACE_HH
#define CONTROLLER_IFACE_HH

#include "define_LT.hh"
#include "systemc-ams.h"

SCA_TDF_MODULE(controller_iface)
{
  public:
    sca_tdf::sca_out<COMMAND> out_command;
    sca_tdf::sca_out<double> out_thresh;

    sca_tdf::sca_de::sca_in<COMMAND> inp_command;
    sca_tdf::sca_de::sca_in<double> inp_thresh;

    controller_iface(sc_core::sc_module_name);

    ~controller_iface();

    void set_attribute();
    void processing();
};

#endif // CONTROLLER_IFACE_HH
