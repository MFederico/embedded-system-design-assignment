#ifndef VALVE_TDF_HH
#define VALVE_TDF_HH
#define REAL

#include "define_LT.hh"
#include "systemc-ams.h"

SCA_TDF_MODULE(valve_tdf)
{
  public:
    
    sca_tdf::sca_in<COMMAND> command;
    sca_tdf::sca_in<double> threshold; 
    sca_tdf::sca_out<double> out;      
    
    SCA_CTOR(valve_tdf);
    ~valve_tdf();

    void set_attributes();
    void initialize(); 
    void ac_processing();
    void processing();

  private:
    double Ts;
    double number;
    double valve_movement_timestep; 
    double currentAperture;
    double currentThreshold;
};

#endif // CONTROLLER_TDF_HH
