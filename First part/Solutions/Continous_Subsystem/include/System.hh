#ifndef SYSTEM_HH
#define SYSTEM_HH

#include "ControllerIFace.hh"
#include "controller_tlm.hh"
#include "define_LT.hh"
#include "valve_tdf.hh"
#include "valve_transactor.hh"
#include "water_tank_lsf.hh"
#include "water_tank_transactor.hh"

class System : public sc_core::sc_module
{
  public:
    System(sc_core::sc_module_name);
    ~System();
    sc_signal<double>
        sig_out_wt; 
  protected:
    valve_tdf *v;
    valve_transactor *vt;
    controller_tlm *ct;
    water_tank_lsf *wt;
    water_tank_transactor *wtt;

    controller_iface *cif;
    sc_signal<COMMAND> command_vt_ci;
    sc_signal<double> threshold_vt_ci;
    sca_tdf::sca_signal<double>
        sig_out_valve; 

    sca_tdf::sca_signal<COMMAND> comm_cif_valve;
    sca_tdf::sca_signal<double> thresh_cif_valve; 
};

#endif // SYSTEM_HH
