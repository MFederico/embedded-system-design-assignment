#ifndef _controller_tlm_H_
#define _controller_tlm_H_

#include "define_LT.hh"
#include "valve_transactor.hh"
#include "water_tank_transactor.hh"
#include <systemc.h>
#include <tlm.h>

class controller_tlm : public sc_module,
                       public virtual tlm::tlm_bw_transport_if<>,
                       public virtual tlm::tlm_fw_transport_if<>
{

  private:
    SC_HAS_PROCESS(controller_tlm);

    virtual void invalidate_direct_mem_ptr(uint64 start_range,
                                           uint64 end_range);

    virtual void b_transport(tlm::tlm_generic_payload &trans, sc_time &t);

    virtual tlm::tlm_sync_enum nb_transport_bw(tlm::tlm_generic_payload &trans,
                                               tlm::tlm_phase &phase,
                                               sc_time &t);

    virtual tlm::tlm_sync_enum nb_transport_fw(tlm::tlm_generic_payload &trans,
                                               tlm::tlm_phase &phase,
                                               sc_time &t);

    virtual bool get_direct_mem_ptr(tlm::tlm_generic_payload &trans,
                                    tlm::tlm_dmi &dmi_data);

    virtual unsigned int transport_dbg(tlm::tlm_generic_payload &trans);

    sc_time local_time;
    int counter;

    tankstruct tankStruct;

    double threshold;
    double waterLevel;

    void run();

    sc_event timeEvent;

  public:
    tlm::tlm_initiator_socket<> initiator_socket;
    tlm::tlm_target_socket<> target_socket;

    controller_tlm(sc_module_name name);
};

#endif
