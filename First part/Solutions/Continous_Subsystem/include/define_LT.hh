#ifndef DEFINE_LT_HH
#define DEFINE_LT_HH

#include "systemc.h"

typedef enum { IDLE, OPEN, CLOSE } COMMAND;

struct iostruct
{
    COMMAND c;
    double th; // treshold
};

struct tankstruct
{
    double level;
};

#endif // DEFINE_LT_HH
