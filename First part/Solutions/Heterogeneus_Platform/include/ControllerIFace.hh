#ifndef CONTROLLER_IFACE_HH
#define CONTROLLER_IFACE_HH

#include "systemc-ams.h"
#include "define_LT.hh"

SCA_TDF_MODULE( controller_iface )
{
  public:
    
    sca_tdf::sca_out< COMMAND > out_command;
     sca_tdf::sca_out< double > out_thresh;

    sca_tdf::sca_de::sca_in< sc_uint<32> > v0,v1;
    sca_tdf::sca_de::sca_in< double > inp_thresh;
    

    controller_iface( sc_core::sc_module_name );

    ~controller_iface();

    void set_attribute();
    void processing();

};

#endif //CONTROLLER_IFACE_HH
