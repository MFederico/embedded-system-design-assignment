#ifndef _XTEA_H_
#define _XTEA_H_

#include <systemc.h>
#define DELTA 0x9e3779b9

class xteaModule : sc_core::sc_module
{
  public:
    sc_in<bool> clk;
    sc_in<sc_logic> rst;

    sc_in<sc_uint<32> > w0, w1, k0, k1, k2, k3;

    sc_in<bool> mode;
    sc_in<sc_logic> din_rdy;

    sc_out<sc_uint<32> > result0, result1;
    sc_out<sc_logic> dout_rdy;

    typedef enum { R, IR, D0, ENC, D1, B, A, D2, C, D, END, DEC } STATES;

    sc_signal<int> STATE, NEXT_STATE;
    SC_HAS_PROCESS(xteaModule);

  private:
    void fsm();
    void dataPath();
    sc_signal<sc_uint<6> > *i;
    sc_signal<sc_uint<2> > *cond_e, *cond_d;
    sc_signal<sc_uint<32> > *keys[4];
    sc_signal<sc_uint<64> > *sum;
    sc_signal<sc_uint<32> > *v0;
    sc_signal<sc_uint<32> > *v1;
    uint64_t delta;

  public:
    xteaModule(const sc_module_name &name);
    ~xteaModule();
};

#endif
