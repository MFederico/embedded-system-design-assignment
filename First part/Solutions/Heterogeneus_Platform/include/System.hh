#ifndef SYSTEM_HH
#define SYSTEM_HH

#include "define_LT.hh"
#include "valve_tdf.hh"
#include "controller_tlm.hh"
#include "valve_transactor.hh"
#include "water_tank_lsf.hh"
#include "water_tank_transactor.hh"
#include "ControllerIFace.hh"
#include "xtea_RTL.hh"

class System : public sc_core::sc_module
{
  public:
    System( sc_core::sc_module_name );
    ~System();
   sc_signal< double > sig_out_wt;      
  protected:
    valve_tdf* v;
    valve_transactor* vt;
    controller_tlm* ct;
    water_tank_lsf* wt;
    water_tank_transactor* wtt;
    xteaModule* xtea;

    controller_iface* cif;

  
    sc_signal< double > threshold_vt_ci; 
    sca_tdf::sca_signal< double > sig_out_valve;  

    sca_tdf::sca_signal< COMMAND > comm_cif_valve;
    sca_tdf::sca_signal< double >thresh_cif_valve; 



    sc_signal<bool> clk;
    sc_signal<sc_logic> rst;

    sc_signal<sc_uint<32> > w0, w1, k0, k1, k2, k3;

    sc_signal<bool> mode;
    sc_signal<sc_logic> din_rdy;

    sc_signal<sc_uint<32> > result0, result1;
    sc_signal<sc_logic> dout_rdy;

   

   
   

};

#endif //SYSTEM_HH
