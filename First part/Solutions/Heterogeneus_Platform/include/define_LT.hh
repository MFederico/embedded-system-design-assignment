#ifndef DEFINE_LT_HH
#define DEFINE_LT_HH

#include "systemc.h"

typedef enum { IDLE, OPEN, CLOSE } COMMAND;

struct iostruct {
	sc_uint<32> w0;
    sc_uint<32> w1;
	double th;		// treshold
};


struct tankstruct
{
	double level;	
};


#endif //DEFINE_LT_HH
