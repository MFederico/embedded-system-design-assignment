#ifndef WATER_TANK_LSF_HH
#define WATER_TANK_LSF_HH

#include <systemc-ams.h>

class water_tank_lsf : public sc_core::sc_module
{

  public:
    sca_tdf::sca_in< double > in;
    sc_core::sc_out< double > out;

    water_tank_lsf( sc_core::sc_module_name );
    ~water_tank_lsf();

  protected:

    sca_lsf::sca_tdf::sca_source source; 
    sca_lsf::sca_de::sca_sink sink;	

    sca_lsf::sca_gain gainer1,gainer2; 
    sca_lsf::sca_add adder;
    sca_lsf::sca_integ integrator;
   
    
    sca_lsf::sca_signal input_gainer1,gainer1_sum,gainer2_sum,sum_integ,integ_gain2;
   
};

#endif
