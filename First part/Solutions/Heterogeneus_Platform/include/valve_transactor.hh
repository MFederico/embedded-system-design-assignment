#ifndef VALVE_TRANSACTOR_HH
#define VALVE_TRANSACTOR_HH


#include <tlm.h>
#include "define_LT.hh"
#include "systemc-ams.h"
#define PERIOD 20

class valve_transactor :
  public sc_module,
  public virtual tlm::tlm_fw_transport_if<>
{
  public:
    tlm::tlm_target_socket<> target_socket; 

    virtual void b_transport( tlm::tlm_generic_payload& trans, sc_time& t );
    virtual bool get_direct_mem_ptr( tlm::tlm_generic_payload& trans, tlm::tlm_dmi & dmi_data );

    virtual unsigned int transport_dbg( tlm::tlm_generic_payload& trans );

    virtual tlm::tlm_sync_enum nb_transport_fw(
        tlm::tlm_generic_payload& trans,
        tlm::tlm_phase& phase,
        sc_time& t);

    void end_of_elaboration();
    void reset();

    sc_event event;

    iostruct ioDataStruct;

    tlm::tlm_generic_payload* pending_transaction;

    sc_time timing_annotation;

    void transduce();

    SC_HAS_PROCESS(valve_transactor);

    valve_transactor(sc_module_name name);
    ~valve_transactor();
   
    sc_out<double > out_thresh;
    
    sc_out<bool> clk;
    sc_out<sc_logic> rst;

    sc_out<sc_uint<32> > w0, w1, k0, k1, k2, k3;

    sc_out<sc_logic> din_rdy;
    sc_in<sc_logic> dout_rdy;

     sc_out<bool> mode;

   void clk_gen();
    
};


#endif 
