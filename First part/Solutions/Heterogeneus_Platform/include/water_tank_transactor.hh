#ifndef WATER_TANK_TRANSACTOR_HH
#define WATER_TANK_TRANSACTOR_HH


#include <tlm.h>
#include "define_LT.hh"
#include "systemc-ams.h"


class water_tank_transactor :
  public sc_module,
  public virtual tlm::tlm_bw_transport_if<>
{
  public:
    tlm::tlm_initiator_socket<> initiator_socket; 

    virtual void invalidate_direct_mem_ptr(uint64 start_range, uint64 end_range);
    
    virtual tlm::tlm_sync_enum nb_transport_bw( tlm::tlm_generic_payload& trans, tlm::tlm_phase& phase, sc_time& t);


    void end_of_elaboration();
    void reset();
    void wait();      


    tankstruct tankStruct;

    tlm::tlm_generic_payload* pending_transaction;

    sc_time timing_annotation;

    SC_HAS_PROCESS(water_tank_transactor);

    water_tank_transactor(sc_module_name name);
    ~water_tank_transactor();

    sc_core::sc_in< double > in;

    

};


#endif 
