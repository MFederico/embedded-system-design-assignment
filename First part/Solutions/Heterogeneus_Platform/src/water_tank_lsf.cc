#include "water_tank_lsf.hh"

water_tank_lsf::water_tank_lsf(sc_core::sc_module_name)
    : source("source")
    , sink("sink")
    , gainer1("gainer1", 0.6)
    , gainer2("gainer2", 0.03)
    , integrator("integrator", 1.0, 0.0)//scale = 1, initial condition = 0
    , 
    adder("adder", 1.0, -1.0) //model 0.6a(t)-0.03x(t)
    , 
    input_gainer1("input_gainer1")
    , gainer1_sum("gainer1_sum")
    , gainer2_sum("gainer2_sum")
    , sum_integ("sum_integ")
    , integ_gain2("integ_gain2")
    , in("in")
    , out("out")
{

	//Connect the LSF blocks	

    source.inp(in); 
    source.y(input_gainer1);

    gainer1.x(input_gainer1);
    gainer1.y(gainer1_sum);

    adder.x1(gainer1_sum);
    adder.x2(gainer2_sum);
    adder.y(sum_integ);

    integrator.x(sum_integ);
    integrator.y(integ_gain2);

    gainer2.x(integ_gain2);
    gainer2.y(gainer2_sum);

   
    sink.x(integ_gain2);
    sink.outp(out);
}

water_tank_lsf::~water_tank_lsf() {}
