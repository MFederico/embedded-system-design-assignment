#include "System.hh"

System::System(sc_core::sc_module_name)
    : threshold_vt_ci("threshold_vt_ci")
    , sig_out_valve("valve-tank")
    , sig_out_wt("wt-transactor")
    , comm_cif_valve("comm_cif_valve")
    , thresh_cif_valve("thresh_cif_valve")
    , clk("clk")
    , rst("rst")
    , w0("w0")
    , w1("w1")
    , k0("k0")
    , k1("k1")
    , k2("k2")
    , k3("k3")
    , mode("mode")
    , din_rdy("din_rdy")
    , dout_rdy("dout_rdy")
    , result0("res0")
    , result1("res1")

{

    v = new valve_tdf("valve_tdf");
    ct = new controller_tlm("controller_tlm");
    vt = new valve_transactor("valve_transactor");
    wt = new water_tank_lsf("water_tank_lsf");
    wtt = new water_tank_transactor("water_tank_transactor");
    cif = new controller_iface("cif");
    xtea = new xteaModule("xtea");

    // bind valve transactor
    vt->clk(clk);
    vt->rst(rst);
    vt->w0(w0);
    vt->w1(w1);
    vt->k0(k0);
    vt->k1(k1);
    vt->k2(k2);
    vt->k3(k3);
    vt->din_rdy(din_rdy);
    vt->dout_rdy(dout_rdy);
    vt->mode(mode);
    vt->out_thresh(threshold_vt_ci);

    // bind xtea
    xtea->clk(clk);
    xtea->rst(rst);
    xtea->w0(w0);
    xtea->w1(w1);
    xtea->k0(k0);
    xtea->k1(k1);
    xtea->k2(k2);
    xtea->k3(k3);
    xtea->result0(result0);
    xtea->result1(result1);
    xtea->din_rdy(din_rdy);
    xtea->dout_rdy(dout_rdy);
    xtea->mode(mode);

    cif->inp_thresh(threshold_vt_ci);
    cif->v0(result0);
    cif->v1(result1);
	cif->out_command(comm_cif_valve);
	cif->out_thresh(thresh_cif_valve);

   // Bind valve
    v->command(comm_cif_valve);
    v->threshold(thresh_cif_valve);
	v->out(sig_out_valve);

    // Controller TLM
    ct->initiator_socket(vt->target_socket);
    

   //Bind water tank
    wt->in(sig_out_valve);
    wt->out(sig_out_wt);

    // Bind Water Tank Transactor
    wtt->in(sig_out_wt);
    wtt->initiator_socket(ct->target_socket);

    
}

System::~System()
{
    delete v;
    delete vt;
    delete ct;
    delete wt;
    delete wtt;
    delete cif;
    delete xtea;
}
