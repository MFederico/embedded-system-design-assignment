#include "ControllerIFace.hh"

controller_iface::controller_iface(sc_core::sc_module_name)
    :

    v0("v0")
    , v1("v1")
    , inp_thresh("inp_thresh")
    , out_command("out_command")
    , out_thresh("out_thresh")

{
}

controller_iface::~controller_iface() {}

void controller_iface::processing()
{

    int a = v0.read();

    out_command.write((COMMAND)a);
    out_thresh.write(inp_thresh.read());
}

void controller_iface::set_attribute() {}
