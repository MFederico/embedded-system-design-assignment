#include "controller_tlm.hh"

controller_tlm::controller_tlm(sc_module_name name)
    : sc_module(name)
    , initiator_socket("initiator_socket")
    , target_socket("target_socket")
    , threshold(0.7)
    , waterLevel(0.0)
{

    initiator_socket(*this);
    target_socket(*this);
    SC_THREAD(run);
}

void controller_tlm::invalidate_direct_mem_ptr(uint64 start_range,
                                               uint64 end_range)
{
}

bool controller_tlm::get_direct_mem_ptr(tlm::tlm_generic_payload &trans,
                                        tlm::tlm_dmi &dmi_data)
{
    return false;
}

tlm::tlm_sync_enum
controller_tlm::nb_transport_bw(tlm::tlm_generic_payload &trans,
                                tlm::tlm_phase &phase, sc_time &t)
{
    return tlm::TLM_COMPLETED;
}

tlm::tlm_sync_enum
controller_tlm::nb_transport_fw(tlm::tlm_generic_payload &trans,
                                tlm::tlm_phase &phase, sc_time &t)
{
    return tlm::TLM_COMPLETED;
}

unsigned int controller_tlm::transport_dbg(tlm::tlm_generic_payload &trans)
{
    return 0;
}

void controller_tlm::b_transport(tlm::tlm_generic_payload &trans, sc_time &t)
{
    
    tankStruct = *((tankstruct *)trans.get_data_ptr());
    trans.set_data_ptr((unsigned char *)&tankStruct);
    waterLevel = tankStruct.level;
   
}

void controller_tlm::xtea_encrypt(sc_uint<32> word0, sc_uint<32> word1,
                                  sc_uint<32> key0, sc_uint<32> key1,
                                  sc_uint<32> key2, sc_uint<32> key3,
                                  sc_uint<32> *res0, sc_uint<32> *res1)
{
    sc_uint<64> sum;
    sc_uint<32> i, delta, v0, v1, temp;
    v0 = word0;
    v1 = word1;
    sum = 0;
    delta = 0x9e3779b9;

    for (i = 0; i < 32; i++)
    {

        if ((sum & 3) == 0)
            temp = key0;
        else if ((sum & 3) == 1)
            temp = key1;
        else if ((sum & 3) == 2)
            temp = key2;
        else
            temp = key3;

        v0 += (((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + temp);

        sum += delta;

        if (((sum >> 11) & 3) == 0)
            temp = key0;
        else if (((sum >> 11) & 3) == 1)
            temp = key1;
        else if (((sum >> 11) & 3) == 2)
            temp = key2;
        else
            temp = key3;

        v1 += (((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + temp);
    }

    *res0 = v0;
    *res1 = v1;
}

void controller_tlm::run()
{
    iostruct valve_packet;
    tlm::tlm_generic_payload payload;
    local_time = SC_ZERO_TIME;
    sc_uint<32> v0, v1;
    sc_uint<32> command;
    
    while (true)
    {
        
        if (waterLevel > 8.8 ) {
            
            command = CLOSE;
            xtea_encrypt(command, 0x00000000, 0x6a1d78c8, 0x8c86d67f,
                         0x2a65bfbe, 0xb4bd6e46, &v0, &v1);
            valve_packet.w0 = v0;
            valve_packet.w1 = v1;
            threshold *= 0.7;
            valve_packet.th = threshold;
           
            wait(5, SC_SEC);
        }

        else if (waterLevel < 5.0)
        {

            command = OPEN;
            xtea_encrypt(command, 0x00000000, 0x6a1d78c8, 0x8c86d67f,
                         0x2a65bfbe, 0xb4bd6e46, &v0, &v1);
            valve_packet.w0 = v0;
            valve_packet.w1 = v1;
            threshold *= 1.1;
            valve_packet.th = threshold;

            wait(5, SC_SEC);
            
        }

        else
        {

            command = IDLE;
            xtea_encrypt(command, 0x00000000, 0x6a1d78c8, 0x8c86d67f,
                         0x2a65bfbe, 0xb4bd6e46, &v0, &v1);
            valve_packet.w0 = v0;
            valve_packet.w1 = v1;
            valve_packet.th = threshold;
        }

        payload.set_data_ptr((unsigned char *)&valve_packet);
        payload.set_address(0);
        payload.set_write();
        initiator_socket->b_transport(payload, local_time);
        wait(1, SC_MS);
       
    }
}
