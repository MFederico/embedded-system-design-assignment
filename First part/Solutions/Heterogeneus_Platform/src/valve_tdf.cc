#include "valve_tdf.hh"

valve_tdf::valve_tdf(sc_core::sc_module_name)
    : command("command")
    , threshold("threshold")
    , out("out")
    , Ts(0.2)
    , 
    currentAperture(0.0)
    , number(0)
    , currentThreshold(0.7)
{
}

valve_tdf::~valve_tdf() {}

void valve_tdf::initialize() {}

void valve_tdf::set_attributes()
{

    set_timestep(sca_core::sca_time(Ts, sc_core::SC_MS));

    command.set_rate(1);
    threshold.set_rate(1);

    out.set_rate(1);
    out.set_delay(1); 
   
}

void valve_tdf::ac_processing() { cout << "valve AC processing" << endl; }

void valve_tdf::processing()
{

    
    valve_movement_timestep = 0.25 * get_timestep().to_seconds();

    currentThreshold = threshold.read();
    switch (command.read())
    {
        case OPEN:
           
            if (currentAperture + valve_movement_timestep <= currentThreshold) {
                currentAperture += valve_movement_timestep;
            }
            out.write(currentAperture);
            break;

        case CLOSE:

            if (currentAperture - valve_movement_timestep >= 0.0) {
                currentAperture -= valve_movement_timestep;
            }

            out.write(currentAperture);
            break;

        case IDLE:
            out.write(currentAperture);
            break;

        default:
            break;
    }
   
}
