#include "valve_transactor.hh"

valve_transactor::valve_transactor(sc_module_name name)
    : sc_module(name)
    , target_socket("target_socket")
    , pending_transaction(NULL)
    , out_thresh("out_thresh")
    , clk("clk")
    , rst("rst")
    , w0("w0")
    , w1("w1")
    , k0("k0")
    , k1("k1")
    , k2("k2")
    , k3("k3")
    , mode("mode")
    , din_rdy("din_rdy")
    , dout_rdy("dout_rdy")
    , event("event")
{
    target_socket(*this);
    SC_THREAD(clk_gen);
}

void valve_transactor::clk_gen()
{
    while (true)
    {
        clk.write(true);
        wait(PERIOD / 2, SC_US);
        clk.write(false);
        wait(PERIOD / 2, SC_US);
    }
}

void valve_transactor::b_transport(tlm::tlm_generic_payload &trans, sc_time &t)
{
    
    ioDataStruct = *((iostruct *)trans.get_data_ptr());
    transduce();
    trans.set_data_ptr((unsigned char *)&ioDataStruct);
}

bool valve_transactor::get_direct_mem_ptr(tlm::tlm_generic_payload &trans,
                                          tlm::tlm_dmi &dmi_data)
{
    return false;
}

tlm::tlm_sync_enum
valve_transactor::nb_transport_fw(tlm::tlm_generic_payload &trans,
                                  tlm::tlm_phase &phase, sc_time &t)
{
    return tlm::TLM_COMPLETED;
}

unsigned int valve_transactor::transport_dbg(tlm::tlm_generic_payload &trans)
{
    return 0;
}

void valve_transactor::transduce()
{
    
    w0.write(ioDataStruct.w0);
    w1.write(ioDataStruct.w1);
    k0.write(0x6a1d78c8);
    k1.write(0x8c86d67f);
    k2.write(0x2a65bfbe);
    k3.write(0xb4bd6e46);
    rst.write(sc_logic(0));
    mode.write(1); // decrypt
    din_rdy.write(sc_logic(1));
    wait(PERIOD, SC_NS);

	//wait for the xtea module to finish decrypt
    while (dout_rdy.read() != sc_logic(1))
    {
        wait(PERIOD, SC_US);
    } 
    din_rdy.write(sc_logic(0));

    wait(PERIOD, SC_US);
	//write threshold to output port
    out_thresh.write(ioDataStruct.th);

}

void valve_transactor::end_of_elaboration() {}

void valve_transactor::reset() {}

valve_transactor::~valve_transactor() {}
