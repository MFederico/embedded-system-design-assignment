#include "System.hh"
#define TRACING //uncomment to enable tracing

int sc_main(int ac, char *av[])
{
    sc_core::sc_set_time_resolution( 1.0, sc_core::SC_US );
    System *s = new System("system");
    
#ifdef TRACING
    sc_trace_file *fp = sc_create_vcd_trace_file("wave");
    sc_trace(fp, s->sig_out_wt, "waterlevel");
#endif
    sc_start(350, SC_SEC);
    delete s;

#ifdef TRACINIG
    sc_close_vcd_trace_file(fp);
#endif
}
