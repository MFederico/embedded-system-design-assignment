LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.NUMERIC_BIT.ALL;


PACKAGE xtea_pack IS
  CONSTANT SIZE_32 : INTEGER := 32;
  type STATES is (R, IR, D0, ENC, D1, B, A, D2, C, D, END_S, DEC);
  type KEYS is array(3 downto 0) of UNSIGNED (SIZE_32-1 DOWNTO 0);
END xtea_pack;


LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.NUMERIC_BIT.ALL;
USE WORK.xtea_pack.ALL;


entity xtea is
	port (	clock : in  bit;
		reset   : in  bit;
		mode : in  bit;
		w0    : in  UNSIGNED (SIZE_32-1 DOWNTO 0);
		w1    : in  UNSIGNED (SIZE_32-1 DOWNTO 0);
		k0_port    : in  UNSIGNED (SIZE_32-1 DOWNTO 0);
		k1_port    : in  UNSIGNED (SIZE_32-1 DOWNTO 0);
		k2_port    : in  UNSIGNED (SIZE_32-1 DOWNTO 0);
		k3_port    : in  UNSIGNED (SIZE_32-1 DOWNTO 0);
		din_rdy : in  bit;
		result0    : out UNSIGNED (SIZE_32-1 DOWNTO 0) := (others=>'0');
		result1    : out UNSIGNED (SIZE_32-1 DOWNTO 0) := (others=>'0');
		dout_rdy : out  bit := '0'
	) ;
end xtea;

architecture tea of xtea is
	signal STATE: STATES:=R;
	signal NEXT_STATE: STATES:=R;
	signal sum: UNSIGNED (63 DOWNTO 0):=(others=>'0');
	signal i: UNSIGNED (5 DOWNTO 0):=(others=>'0');
	signal cond_e, cond_d : UNSIGNED (1 DOWNTO 0):=(others=>'0');
	signal v0: UNSIGNED (SIZE_32-1 DOWNTO 0):=(others=>'0');
	signal v1: UNSIGNED (SIZE_32-1 DOWNTO 0):=(others=>'0');
	signal keys_array : KEYS;
	CONSTANT DELTA : UNSIGNED (SIZE_32-1 DOWNTO 0) := "10011110001101110111100110111001";
begin

--processo FSM
process(STATE, din_rdy,mode,i) 
	begin
		case STATE is
		  when R => 
			if din_rdy = '1' then
			    NEXT_STATE<=IR;
			else
			    NEXT_STATE<=R;
			end if;
		  when IR => 
			NEXT_STATE<=D0;
		  when D0   =>
			if mode = '1' then 
			    NEXT_STATE<=DEC;
			else
			    NEXT_STATE<=ENC;
			end if;
		  when ENC   =>
			if i < 32 then
		            NEXT_STATE<=A;
			else
			    NEXT_STATE<=END_S;
			end if;
		  when A  =>
			NEXT_STATE<=D1;
		  when D1  =>
			NEXT_STATE<=B;
		  when B  =>
			NEXT_STATE<=ENC;
		  when DEC   =>
			if i < 32 then 
			    NEXT_STATE<=C;
			else
			    NEXT_STATE<=END_S;
			end if;
		  when C  =>
			NEXT_STATE<=D2;
		  when D2  =>
			NEXT_STATE<=D;
		  when D  =>
			NEXT_STATE<=DEC;
		  when END_S  =>
			NEXT_STATE<=R;
		  when others =>
			NEXT_STATE<=R;
		end case;
	end process;

--processo datapath
process(clock, reset) 
	variable acc: UNSIGNED (SIZE_32-1 DOWNTO 0):= (others=>'0');
	begin
	  if reset='1' or din_rdy='0' then 
		STATE <= R;
	  elsif clock'event and clock='1' then 
		STATE<=NEXT_STATE;
		case STATE is
		  when R => 
			sum <= (others=>'0');
			i <= (others=>'0');
			cond_e <= (others=>'0');
			cond_d <= (others=>'0');
			dout_rdy <= '0';
            result0 <= (others=>'0');
			result1 <= (others=>'0');
			v0 <= (others=>'0');
			v1 <= (others=>'0');
		  when IR => 
			v0 <= w0;
			v1 <= w1;
			keys_array(0)<= k0_port;
			keys_array(1)<= k1_port;
			keys_array(2)<= k2_port;
			keys_array(3)<= k3_port;
		  when D0 =>
         		cond_e <= RESIZE((sum and RESIZE("011",64)),2);
			cond_d <= RESIZE((((DELTA * 32) srl 11) and RESIZE("011",64)),2); 
		  when D1 =>
			i <= i+1;
			cond_e <= RESIZE(((sum srl 11) and RESIZE("011",64)),2);
		  when DEC   =>
			sum <= RESIZE(DELTA * (32-i),64);
			i <= i+1;
		  when ENC  =>
       			null;
		  when D2  =>
			cond_d <= RESIZE(sum and RESIZE("011",64),2);
		  when A  =>
			acc:=RESIZE(RESIZE((((v1 sll 4) xor (v1 srl 5)) + v1),64) xor 
			(sum + keys_array(to_integer(cond_e))),32);
			acc:=acc+v0;
			v0 <= acc;
			acc:=RESIZE(sum,32);
			sum<=RESIZE((acc+DELTA),64);
			
	  	  when B  =>
			acc:=RESIZE(RESIZE((((v0 sll 4) xor (v0 srl 5)) + v0),64) xor 
			(sum + keys_array(to_integer(cond_e))),32);
			acc:=acc+v1;
			v1 <= acc;
			cond_e<= RESIZE(sum and RESIZE("011",64),2);
	          when C  =>
			acc:=RESIZE(RESIZE((((v0 sll 4) xor (v0 srl 5)) + v0),64) xor 
			(sum + keys_array(to_integer(cond_d))),32);
			acc:=v1-acc;
			v1 <= acc;
			sum <= RESIZE(DELTA * (32 -i),64);
		  when D =>
			acc:=RESIZE(RESIZE((((v1 sll 4) xor (v1 srl 5)) + v1),64) xor 
			(sum + keys_array(to_integer(cond_d))),32);
			acc:=v0-acc;
			v0 <= acc;
			cond_d <= RESIZE((sum srl 11) and RESIZE("011",64),2);
		  when END_S =>
			dout_rdy<='1';
			result0<=v0;
			result1<=v1;
		  when OTHERS =>
			dout_rdy <= '0';
                        result0 <= (others=>'0');
			result1 <= (others=>'0');
	  end case;
	  end if;
end process;
	
end tea;