# Embedded System Design - Final assignment
This repository contains the code and reports for the final assignment of the Embedded System Design class at University of Verona.

# Project Specifications
The goal of the project is to design and test a HW/SW system for controlling the valve of a water tank in order to keep the water level between a given range of values, taking account of the tank's fixed water loss.\
Commands sent to the valve can be of three kinds: OPEN, CLOSE and IDLE. Each command is encrypted before being sent and decrypted once received, using the xTEA encryption algorithm, which is also implemented in the design.\

## First part
The first part of the project required to design the xTEA component and the valve system using SystemC, and consists of the following sections:

-  SystemC code used to design the xTEA component at an RTL level 
-  SystemC code used to implement the encrypt and decrypt operations at TLM level
-  SystemC AMS code used to implement the whole system made by the controller, valve and water tank
-  Code implementing the whole heterogeneus platfrom made by controller, xTEA module, valve and water tank

## Second part
The second part of the project required to design a synthesizable VHDL XTEA Cypher/Decypher and to exploit automatic abstraction and the FMI standard to build cycle-accurate Virtual Platforms from heterogeneous IPs
